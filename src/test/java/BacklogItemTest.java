import org.junit.Assert;
import org.junit.Test;
import projectmanagement.Activity;
import projectmanagement.Developer;
import projectmanagement.backlogitem.BacklogItem;
import projectmanagement.notificator.WhatsappAdapter;

public class BacklogItemTest {

    @Test
    public void backlog_item_should_be_created() {
        var backlogItem = new BacklogItem();
        Assert.assertNotNull(backlogItem);
        Assert.assertFalse(backlogItem.isDone());
        Assert.assertFalse(backlogItem.isTestingDone());
        Assert.assertNull(backlogItem.getDeveloper());
        Assert.assertNull(backlogItem.getTester());
        Assert.assertNotNull(backlogItem.getState());
    }

    @Test
    public void backlog_item_developer_should_be_added() {
        var backlogItem = new BacklogItem();
        var developer = new Developer("John", false, false, new WhatsappAdapter());
        backlogItem.assignDeveloper(developer);

        Assert.assertEquals(developer, backlogItem.getDeveloper());
    }

    @Test
    public void backlog_item_should_return_list_of_developers_when_activity_exists() {
        var backlogItem = new BacklogItem();
        var developer = new Developer("John", false, false, new WhatsappAdapter());
        var activity = new Activity("activity fitting for backlog", "some description");
        backlogItem.addActivity(activity);

        backlogItem.assignDeveloper(developer);
        Assert.assertNotEquals(developer, backlogItem.getDeveloper());

        backlogItem.assignDeveloperToActivity(developer, activity);
        Assert.assertEquals(developer, activity.getDeveloper());
    }
}
