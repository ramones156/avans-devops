import org.junit.Assert;
import org.junit.Test;
import projectmanagement.Activity;
import projectmanagement.ActivityThread;
import projectmanagement.Developer;
import projectmanagement.Reply;
import projectmanagement.backlogitem.BacklogItem;
import projectmanagement.notificator.WhatsappAdapter;

public class BacklogItemStateTest {

    @Test
    public void backlog_item_should_be_created_with_todo_state() {
        var backlogItem = new BacklogItem();

        Assert.assertEquals(backlogItem.getState(), backlogItem.todoState);
    }

    @Test
    public void backlog_item_should_not_be_able_to_mark_complete_when_in_todo_state() {
        var backlogItem = new BacklogItem();
        backlogItem.setCompleted(true);
        Assert.assertEquals(backlogItem.getState(), backlogItem.todoState);
        Assert.assertNotEquals(backlogItem.getState(), backlogItem.doingState);
        Assert.assertNotEquals(backlogItem.getState(), backlogItem.readyForTestingState);
    }

    @Test
    public void backlog_item_should_go_to_doing_when_assigned() {
        var backlogItem = new BacklogItem();
        var developer = new Developer("John", false, false, new WhatsappAdapter());
        backlogItem.assignDeveloper(developer);
        Assert.assertEquals(backlogItem.getState(), backlogItem.doingState);
    }

    @Test
    public void backlog_item_should_go_to_ready_for_testing_when_marked_complete() {
        var backlogItem = new BacklogItem();
        var developer = new Developer("John", false, false, new WhatsappAdapter());

        backlogItem.assignDeveloper(developer);
        backlogItem.setCompleted(true);

        Assert.assertEquals(backlogItem.getState(), backlogItem.readyForTestingState);
    }

    @Test
    public void backlog_item_should_go_to_testing_when_tester_is_assigned() {
        var backlogItem = new BacklogItem();
        var developer = new Developer("John", false, false, new WhatsappAdapter());

        backlogItem.assignDeveloper(developer);
        backlogItem.setCompleted(true);

        var tester = new Developer("Jane", true, false, new WhatsappAdapter());
        backlogItem.assignTester(tester);

        Assert.assertEquals(backlogItem.getState(), backlogItem.testingState);
    }

    @Test
    public void backlog_item_should_not_go_to_testing_when_tester_is_not_a_tester() {
        var backlogItem = new BacklogItem();
        var developer = new Developer("John", false, false, new WhatsappAdapter());

        backlogItem.assignDeveloper(developer);
        backlogItem.setCompleted(true);

        var tester = new Developer("Jane", false, false, new WhatsappAdapter());
        backlogItem.assignTester(tester);

        Assert.assertNotEquals(backlogItem.getState(), backlogItem.testingState);
        Assert.assertEquals(backlogItem.getState(), backlogItem.readyForTestingState);
    }

    @Test
    public void backlog_item_should_go_to_tested_when_tester_is_done() {
        var backlogItem = new BacklogItem();
        var developer = new Developer("John", false, false, new WhatsappAdapter());

        backlogItem.assignDeveloper(developer);
        backlogItem.setCompleted(true);

        var tester = new Developer("Jane", true, false, new WhatsappAdapter());
        backlogItem.assignTester(tester);
        backlogItem.setTestSuccessful(true);

        Assert.assertEquals(backlogItem.getState(), backlogItem.testedState);
    }

    @Test
    public void backlog_item_should_not_add_activity_when_state_is_not_todo_or_doing() {
        var backlogItem = new BacklogItem();
        var developer = new Developer("John", false, false, new WhatsappAdapter());
        var activity = new Activity("name", "desc");

        //in todostate
        backlogItem.addActivity(activity);
        Assert.assertTrue(backlogItem.getActivities().size() > 0);
        backlogItem.removeActivity(activity);
        Assert.assertEquals(0, backlogItem.getActivities().size());

        backlogItem.assignDeveloper(developer);
        // in doingstate
        backlogItem.addActivity(activity);
        Assert.assertTrue(backlogItem.getActivities().size() > 0);
        backlogItem.removeActivity(activity);
        Assert.assertEquals(0, backlogItem.getActivities().size());

        backlogItem.setCompleted(true);
        // in readyfortesting
        backlogItem.addActivity(activity);
        Assert.assertFalse(backlogItem.getActivities().size() > 0);
        backlogItem.removeActivity(activity);
        Assert.assertEquals(0, backlogItem.getActivities().size());
    }

    @Test
    public void backlog_item_should_assign_developer_to_activity() {
        var backlogItem = new BacklogItem();
        var developer = new Developer("John", false, true, new WhatsappAdapter());
        var activity = new Activity("name", "desc");

        Assert.assertNull(activity.getDeveloper());

        backlogItem.addActivity(activity);
        backlogItem.assignDeveloperToActivity(developer, activity);

        Assert.assertEquals(activity.getDeveloper(), developer);
    }

    @Test
    public void backlog_item_should_not_add_activity_thread_when_state_is_finished() {
        var backlogItem = new BacklogItem();
        var developer = new Developer("John", false, true, new WhatsappAdapter());
        var activitythread = new ActivityThread("", developer);

        //in todostate
        backlogItem.addActivityThread(activitythread);
        Assert.assertTrue(backlogItem.getActivityThreads().size() > 0);
        backlogItem.removeActivityThread(activitythread);
        Assert.assertEquals(0, backlogItem.getActivityThreads().size());

        backlogItem.assignDeveloper(developer);
        backlogItem.setCompleted(true);

        var tester = new Developer("Jane", true, false, new WhatsappAdapter());
        backlogItem.assignTester(tester);
        backlogItem.setTestSuccessful(true);
        backlogItem.setDone(developer, true);
        //in finishedstate
        backlogItem.addActivityThread(activitythread);
        Assert.assertFalse(backlogItem.getActivityThreads().size() > 0);
        backlogItem.removeActivityThread(activitythread);
        Assert.assertEquals(0, backlogItem.getActivityThreads().size());
    }

    @Test
    public void backlog_item_should_not_add_replies_to_thread_when_state_is_finished() {
        var backlogItem = new BacklogItem();
        var developer = new Developer("John", false, true, new WhatsappAdapter());
        var activityThread = new ActivityThread("Some question", developer);
        var reply = new Reply("Some reply", developer);

        //in todostate
        backlogItem.addActivityThread(activityThread);

        backlogItem.addReplyToActivityThread(0, reply);
        Assert.assertEquals(1, activityThread.getReplies().size());
        backlogItem.removeReplyToActivityThread(0, reply);
        Assert.assertEquals(0, activityThread.getReplies().size());

        backlogItem.assignDeveloper(developer);
        backlogItem.setCompleted(true);

        var tester = new Developer("Jane", true, false, new WhatsappAdapter());
        backlogItem.assignTester(tester);
        backlogItem.setTestSuccessful(true);
        backlogItem.setDone(developer, true);

        //in finishedstate
        backlogItem.addReplyToActivityThread(0, reply);
        Assert.assertEquals(0, activityThread.getReplies().size());
        backlogItem.removeReplyToActivityThread(0, reply);
        Assert.assertEquals(0, activityThread.getReplies().size());
    }

    @Test
    public void backlog_item_cant_go_to_ready_for_testing_when_activities_arent_done() {
        var backlogItem1 = new BacklogItem();
        var backlogItem2 = new BacklogItem();
        var developer = new Developer("John", false, true, new WhatsappAdapter());
        var activity = new Activity("Activity name", "Description");

        backlogItem1.addActivity(activity);

        backlogItem1.assignDeveloper(developer);
        backlogItem2.assignDeveloper(developer);
        backlogItem1.setCompleted(true);
        backlogItem2.setCompleted(true);

        Assert.assertNotEquals(backlogItem1.readyForTestingState, backlogItem1.getState());
        Assert.assertEquals(backlogItem2.readyForTestingState, backlogItem2.getState());
        Assert.assertEquals(backlogItem1.doingState, backlogItem1.getState());

        activity.setDone(true);
        backlogItem1.setCompleted(true);
        Assert.assertEquals(backlogItem1.readyForTestingState, backlogItem1.getState());
    }

    @Test
    public void backlog_item_should_go_back_to_todo_when_tester_finds_error() {
        var backlogItem = new BacklogItem();
        var developer = new Developer("John", false, true, new WhatsappAdapter());

        //in todostate

        backlogItem.assignDeveloper(developer);
        backlogItem.setCompleted(true);

        var tester = new Developer("Jane", true, false, new WhatsappAdapter());
        backlogItem.assignTester(tester);
        Assert.assertEquals(backlogItem.testingState, backlogItem.getState());
        backlogItem.setTestSuccessful(false);
        Assert.assertEquals(backlogItem.todoState, backlogItem.getState());
    }

    @Test
    public void backlog_item_should_go_back_to_todo_when_lead_developer_finds_it_not_done() {
        var backlogItem = new BacklogItem();
        var developer = new Developer("John", false, true, new WhatsappAdapter());

        backlogItem.assignDeveloper(developer);
        backlogItem.setCompleted(true);

        var tester = new Developer("Jane", true, false, new WhatsappAdapter());
        backlogItem.assignTester(tester);
        backlogItem.setTestSuccessful(true);
        Assert.assertEquals(backlogItem.testedState, backlogItem.getState());
        backlogItem.setDone(developer, false);
        Assert.assertEquals(backlogItem.todoState, backlogItem.getState());
    }
}
