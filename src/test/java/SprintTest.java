import org.junit.Assert;
import org.junit.Test;
import projectmanagement.backlogitem.BacklogItem;
import projectmanagement.sprint.DevelopmentSprint;

import java.time.LocalDateTime;

public class SprintTest {

    @Test
    public void sprint_should_format_date_to_week_date_span() {
        var date_on_monday = LocalDateTime.of(2022, 3, 7, 0, 0, 0);
        var sprint1 = new DevelopmentSprint(date_on_monday);
        Assert.assertEquals(date_on_monday.toLocalDate(), sprint1.getStartDate());
        Assert.assertEquals("2022-03-07", sprint1.getStartDate().toString());

        var date_on_tuesday = LocalDateTime.of(2022, 3, 8, 0, 0, 0);
        var sprint2 = new DevelopmentSprint(date_on_tuesday);
        Assert.assertEquals(date_on_monday.toLocalDate(), sprint2.getStartDate());
        Assert.assertEquals("2022-03-07", sprint2.getStartDate().toString());

        var date_on_sunday = LocalDateTime.of(2022, 3, 13, 0, 0, 0);
        var sprint3 = new DevelopmentSprint(date_on_sunday);
        Assert.assertEquals(date_on_monday.toLocalDate(), sprint3.getStartDate());
        Assert.assertEquals("2022-03-07", sprint3.getStartDate().toString());

        var date_on_monday_again = LocalDateTime.of(2022, 3, 14, 0, 0, 0);
        var sprint4 = new DevelopmentSprint(date_on_monday_again);
        Assert.assertNotEquals(date_on_monday.toLocalDate(), sprint4.getStartDate());
        Assert.assertEquals(date_on_monday_again.toLocalDate(), sprint4.getStartDate());
        Assert.assertEquals("2022-03-14", sprint4.getStartDate().toString());
    }

    @Test
    public void sprint_should_have_name_of_sprint_date() {
        var date_on_monday = LocalDateTime.of(2022, 3, 7, 0, 0, 0);
        var sprint1 = new DevelopmentSprint(date_on_monday);
        Assert.assertEquals("MARCH WEEK 1", sprint1.name);

        var date_on_tuesday = LocalDateTime.of(2022, 3, 8, 0, 0, 0);
        var sprint2 = new DevelopmentSprint(date_on_tuesday);
        Assert.assertEquals("MARCH WEEK 1", sprint2.name);

        var date_on_sunday = LocalDateTime.of(2022, 3, 13, 0, 0, 0);
        var sprint3 = new DevelopmentSprint(date_on_sunday);
        Assert.assertEquals("MARCH WEEK 1", sprint3.name);

        var date_on_monday_again = LocalDateTime.of(2022, 3, 14, 0, 0, 0);
        var sprint4 = new DevelopmentSprint(date_on_monday_again);
        Assert.assertEquals("MARCH WEEK 2", sprint4.name);
    }

    @Test
    public void sprint_should_add_backlog_item() {
        var date = LocalDateTime.of(2022, 3, 7, 0, 0, 0);
        var sprint = new DevelopmentSprint(date);
        var backlogItem = new BacklogItem();

        sprint.addBacklogItem(backlogItem);
        Assert.assertEquals(1, sprint.getBacklogItems().size());
        sprint.removeBacklogItem(backlogItem);
        Assert.assertEquals(0, sprint.getBacklogItems().size());
    }
}
