import org.junit.Assert;
import org.junit.Test;
import projectmanagement.sprint.DevelopmentSprint;

import java.time.LocalDateTime;

public class SprintStateTest {


    @Test
    public void sprint_state_should_update_when_start_date_is_today() {
        var sprint = new DevelopmentSprint(LocalDateTime.now());

        Assert.assertEquals(sprint.scheduledState,sprint.getState());
        sprint.update();
        Assert.assertEquals(sprint.inProgressState,sprint.getState());
    }

    @Test
    public void sprint_state_should_update_when_end_date_is_today() {
        var sprint = new DevelopmentSprint(LocalDateTime.now());

        Assert.assertEquals(sprint.scheduledState, sprint.getState());
        sprint.update();
        Assert.assertEquals(sprint.inProgressState, sprint.getState());
        sprint.setStartDate(sprint.getStartDate().minusDays(7));
        sprint.update();
        Assert.assertEquals(sprint.finishedState, sprint.getState());
    }

}
