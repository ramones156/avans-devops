import org.junit.Assert;
import org.junit.Test;
import pipeline.*;

import java.util.ArrayList;

public class PipelineTest {

    @Test
    public void pipeline_should_be_created() {
        var pipeline = new Pipeline();
        Assert.assertNotNull(pipeline);
        Assert.assertNotNull(pipeline.onCommands);
        Assert.assertNotNull(pipeline.offCommands);
    }

    @Test
    public void pipeline_should_add_stages() {
        var pipeline = new Pipeline();
        var commandList = new ArrayList<Command>();
        var pipelineJobs = new ArrayList<PipelineJob>();
        pipelineJobs.add(new PipelineJob());
        commandList.add(new PipelineQueueStageCommand(pipelineJobs));

        var startCommand = new StartPipelineJobMacroCommand(commandList);
        var stopCommand = new StopPipelineJobMacroCommand(commandList);

        pipeline.setCommand(0,startCommand,stopCommand);

        Assert.assertNotEquals(pipeline.onCommands[1],startCommand);
        Assert.assertNotEquals(pipeline.offCommands[1],stopCommand);
        Assert.assertEquals(pipeline.onCommands[0],startCommand);
        Assert.assertEquals(pipeline.offCommands[0],stopCommand);
    }


    @Test
    public void pipeline_should_overwrite_stages() {
        var pipeline = new Pipeline();
        var commandList = new ArrayList<Command>();
        var pipelineJobs = new ArrayList<PipelineJob>();
        pipelineJobs.add(new PipelineJob());
        commandList.add(new PipelineQueueStageCommand(pipelineJobs));

        var startCommand = new StartPipelineJobMacroCommand(commandList);
        var stopCommand = new StopPipelineJobMacroCommand(commandList);

        pipeline.setCommand(0,startCommand,stopCommand);
        pipeline.setCommand(0,startCommand,stopCommand);

        Assert.assertNotEquals(pipeline.onCommands[1],startCommand);
        Assert.assertNotEquals(pipeline.offCommands[1],stopCommand);
        Assert.assertEquals(pipeline.onCommands[0],startCommand);
        Assert.assertEquals(pipeline.offCommands[0],stopCommand);

        pipeline.setCommand(1,startCommand,stopCommand);

        Assert.assertEquals(pipeline.onCommands[1],startCommand);
        Assert.assertEquals(pipeline.offCommands[1],stopCommand);
        Assert.assertNotEquals(pipeline.onCommands[2],startCommand);
        Assert.assertNotEquals(pipeline.offCommands[2],stopCommand);
    }
}
