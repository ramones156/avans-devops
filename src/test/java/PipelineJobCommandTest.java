import org.junit.Assert;
import org.junit.Test;
import pipeline.*;

import java.util.ArrayList;

public class PipelineJobCommandTest {

    @Test
    public void pipeline_job_queue_command_should_queue_every_job() {
        var pipelineJobs = new ArrayList<PipelineJob>();
        pipelineJobs.add(new PipelineJob());
        var pipelineJobQueueCommand = new PipelineQueueStageCommand(pipelineJobs);
        pipelineJobQueueCommand.execute();

        for (var i : pipelineJobQueueCommand.pipelineJobs) {
            Assert.assertEquals(PipelineJob.QUEUED, i.stage);
        }
    }

    @Test
    public void pipeline_job_run_command_should_run_every_job() {
        var pipelineJobs = new ArrayList<PipelineJob>();
        pipelineJobs.add(new PipelineJob());
        var pipelineJobRunCommand = new PipelineRunStageCommand(pipelineJobs);
        pipelineJobRunCommand.execute();

        for (var i : pipelineJobRunCommand.pipelineJobs) {
            Assert.assertEquals(PipelineJob.RUNNING, i.stage);
        }
    }


    @Test
    public void pipeline_job_finish_command_should_finish_every_job() {
        var pipelineJobs = new ArrayList<PipelineJob>();
        pipelineJobs.add(new PipelineJob());
        var pipelineJobRunCommand = new PipelineRunStageCommand(pipelineJobs);
        pipelineJobRunCommand.execute(); // you need the run stage so jobs can finish
        var pipelineJobFinishCommand = new PipelineFinishStageCommand(pipelineJobs);
        pipelineJobFinishCommand.execute();

        for (var i : pipelineJobFinishCommand.pipelineJobs) {
            Assert.assertEquals(PipelineJob.FINISHED, i.stage);
        }
    }

    @Test
    public void start_pipeline_job_command_should_start_all_pipeline_jobs_in_stage() {
        var pipelineJobs = new ArrayList<PipelineJob>();
        var job = new PipelineJob();
        pipelineJobs.add(job);
        var commands = new ArrayList<Command>();
        var pipelineStage = new PipelineQueueStageCommand(pipelineJobs);
        commands.add(pipelineStage);

        var startCommand = new StartPipelineJobMacroCommand(commands);
        startCommand.execute();

        Assert.assertEquals(PipelineJob.QUEUED,job.stage);
    }

    @Test
    public void pipeline_loader_should_load_start_and_stop_commands() {
        var pipelineJobs = new ArrayList<PipelineJob>();
        pipelineJobs.add(new PipelineJob());
        var commands = new ArrayList<Command>();
        var pipelineStage = new PipelineQueueStageCommand(pipelineJobs);
        commands.add(pipelineStage);
        PipelineLoader pipelineLoader = new PipelineLoader(commands);

        Assert.assertNotNull(pipelineLoader.startPipelineJobCommand);
        Assert.assertNotNull(pipelineLoader.stopPipelineJobCommand);
        Assert.assertNotNull(pipelineLoader.startPipelineJobCommand.commands);
        Assert.assertNotNull(pipelineLoader.stopPipelineJobCommand.commands);
        Assert.assertEquals(pipelineJobs.size(),pipelineLoader.startPipelineJobCommand.commands.size());
        Assert.assertEquals(pipelineJobs.size(),pipelineLoader.stopPipelineJobCommand.commands.size());
    }


    @Test
    public void pipeline_undo_should_undo_pipeline() {
        var pipelineJobs = new ArrayList<PipelineJob>();
        var job = new PipelineJob();
        pipelineJobs.add(job);
        var commands = new ArrayList<Command>();

        var pipelineQueueStage = new PipelineQueueStageCommand(pipelineJobs);
        commands.add(pipelineQueueStage);

        var startCommand = new StartPipelineJobMacroCommand(commands);
        startCommand.execute();

        Assert.assertEquals(PipelineJob.QUEUED,job.stage);

        startCommand.undo();
        Assert.assertEquals(PipelineJob.OFF,job.stage);
    }

    @Test
    public void fail_command_should_fail_pipeline() {
        var pipelineJobs = new ArrayList<PipelineJob>();
        var job = new PipelineJob();
        pipelineJobs.add(job);

        var commands = new ArrayList<Command>();

        var pipelineFailStage = new PipelineFailStageCommand(pipelineJobs);
        commands.add(pipelineFailStage);

        var stopCommand = new StopPipelineJobMacroCommand(commands);
        stopCommand.execute();

        Assert.assertEquals(PipelineJob.FAILED,job.stage);

    }

}
