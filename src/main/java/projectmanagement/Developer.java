package projectmanagement;


import projectmanagement.notificator.NotificatorService;

public class Developer {
    String name;
    public boolean isTester;
    public boolean isLead;
    public NotificatorService preferredService;

    public Developer(String name, boolean isTester, boolean isLead, NotificatorService preferredService) {
        this.name = name;
        this.isTester = isTester;
        this.isLead = isLead;
        this.preferredService = preferredService;
    }
}
