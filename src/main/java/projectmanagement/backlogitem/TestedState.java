package projectmanagement.backlogitem;

import projectmanagement.Developer;

public class TestedState extends BacklogItemState {

    public TestedState(BacklogItem backlogItem) {
        super(backlogItem);
    }
    @Override
    void setDone(Developer leadDeveloper, boolean done){

        if (leadDeveloper.isLead) {
            this.backlogItem.done = done;
            if (this.backlogItem.done) {
                this.backlogItem.setState(this.backlogItem.doneState);
            } else {
                this.setTestSuccessful(false);
                this.backlogItem.setState(this.backlogItem.todoState);
            }
        }
    }

}
