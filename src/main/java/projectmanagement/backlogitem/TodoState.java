package projectmanagement.backlogitem;

import projectmanagement.Activity;
import projectmanagement.Developer;

public class TodoState extends BacklogItemState {
    public TodoState(BacklogItem backlogItem) {
        super(backlogItem);
    }

    @Override
    void addActivity(Activity activity) {
        this.backlogItem.activities.add(activity);
    }

    @Override
    void removeActivity(Activity activity) {
        this.backlogItem.activities.remove(activity);
    }

    @Override
    void assignDeveloper(Developer developer) {
        this.backlogItem.developer = developer;
        this.backlogItem.setState(this.backlogItem.doingState);
    }
}
