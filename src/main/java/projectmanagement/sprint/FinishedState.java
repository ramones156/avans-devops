package projectmanagement.sprint;

import projectmanagement.backlogitem.BacklogItem;

public class FinishedState extends SprintState {
    public FinishedState(Sprint sprint) {
        super(sprint);
    }

    @Override
    public void addBacklogItem(BacklogItem backlogItem) {
        // sprint is done and archived, can't edit
    }

    @Override
    public void removeBacklogItem(BacklogItem backlogItem) {
        // sprint is done and archived, can't edit
    }

    @Override
    public void update() {
        // sprint is done and archived, can't edit
    }
}
