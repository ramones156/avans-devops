package projectmanagement.sprint;

import projectmanagement.backlogitem.BacklogItem;


public abstract class SprintState {
    Sprint sprint;
    protected SprintState(Sprint sprint) {
        this.sprint = sprint;
    }
    public void addBacklogItem(BacklogItem backlogItem) {
        sprint.addBacklogItemInState(backlogItem);
    }

    public void removeBacklogItem(BacklogItem backlogItem) {
        sprint.removeBacklogItemInState(backlogItem);
    }
    public abstract void update();
}
