package projectmanagement;

import java.util.ArrayList;
import java.util.List;

public class ActivityThread {
    private final List<Reply> replies = new ArrayList<>();
    String content;
    Developer byDeveloper;

    public ActivityThread(String content, Developer byDeveloper) {
        this.content = content;
        this.byDeveloper = byDeveloper;
    }

    public void addReply(Reply r) {
        this.replies.add(r);
    }

    public void removeReply(Reply r) {
        this.replies.remove(r);
    }

    public List<Reply> getReplies() {
        return replies;
    }
}
