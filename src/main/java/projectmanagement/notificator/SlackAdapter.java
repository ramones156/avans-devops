package projectmanagement.notificator;

import java.util.logging.Logger;

public class SlackAdapter implements NotificatorService {
    Logger logger = Logger.getLogger(String.valueOf(this));

    @Override
    public void sendMessage() {
        logger.info("Sent a message through Slack!");
    }
}
