package codearchive;

import java.util.ArrayList;
import java.util.List;

public class Repository {
    public List<Branch> branches = new ArrayList<>();
    public Branch mainBranch;

    public Repository(String mainBranchName) {
        this.mainBranch = new Branch(mainBranchName);
    }

    public Code getMainCode() {
        return this.mainBranch.getCode();
    }
}
