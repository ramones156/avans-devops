package codearchive;



public interface BranchObservable {
    void registerObserver(BranchObserver o);
    void removeObserver(BranchObserver o);

    void notifyObservers();
}
