package pipeline;


import java.util.ArrayList;

public class StartPipelineJobMacroCommand implements Command {
    public ArrayList<Command> commands;

    public StartPipelineJobMacroCommand(ArrayList<Command> commands) {
        this.commands = commands;
    }

    @Override
    public void execute() {
        for (var i : commands) {
            i.execute();
        }
    }

    @Override
    public void undo() {
        for (var i : commands) {
            i.undo();
        }
    }
}
