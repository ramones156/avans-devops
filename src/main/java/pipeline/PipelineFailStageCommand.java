package pipeline;

import java.util.ArrayList;
import java.util.List;

public class PipelineFailStageCommand implements Command {
    public List<PipelineJob> pipelineJobs;
    public PipelineFailStageCommand(ArrayList<PipelineJob> pipelineJobs) {
        this.pipelineJobs = pipelineJobs;
    }
    @Override
    public void execute() {
        for(var j : pipelineJobs) {
            j.prevStage = j.stage;
            j.stop();
        }
    }
    @Override
    public void undo() {
        for (var j : pipelineJobs) {
            j.stage = j.prevStage;
        }
    }
}
